<?php


function baseUrl($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
{
    if (isset($_SERVER['HTTP_HOST'])) {
        $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
        $hostname = $_SERVER['HTTP_HOST'];
        $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

        $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
        $core = $core[0];

        $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
        $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
        $base_url = sprintf($tmplt, $http, $hostname, $end);
    } else $base_url = 'http://localhost/';

    if ($parse) {
        $base_url = parse_url($base_url);
        if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
    }

    return $base_url;
}

function url($route)
{
    return baseUrl() . $route;
}

function redirect($route)
{
    header("Location: " . baseUrl() . $route);
}

function view($viewFile)
{
    return getcwd() . '/resources/views/' . $viewFile;
}

function encryptPassword($password)
{
    return password_hash($password, PASSWORD_DEFAULT);
}


function checkPassword($password, $hash)
{
    return password_verify($password, $hash);
}

function setError($key, $message)
{
    $_SESSION['errors'][$key] = $message;
}

function setAlert($key, $message)
{
    $_SESSION['alerts'][$key] = $message;
}

function session($key)
{
    return $_SESSION[$key];
}

function errors($key)
{
    if (isset($_SESSION['errors'])) {
        $errors = $_SESSION['errors'];

        if ($key === 'all') {
            return $errors;
        }

        return $errors[$key];
    }
}

function alerts($key)
{
    if (isset($_SESSION['alerts'])) {
        $alerts = $_SESSION['alerts'];

        if ($key === 'all') {
            return $alerts;
        }

        return $alerts[$key];
    }
}

function isAuthed(){
    if (!$_SESSION['authed_user']) {
        redirect('login');
    }
}