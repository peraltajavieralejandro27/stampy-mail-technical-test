<?php


class User
{

    private static $allowed = ["name", "username", "email", "password", "updated_at"];

    private static function prepareFields($fields, $method = 'create')
    {
        $setStr = [];
        foreach (self::$allowed as $key) {
            if (!empty($fields[$key]) || $fields[$key] != "" || $fields[$key] != NULL) {
                switch ($method) {
                    case 'create':
                        $setStr['fields'][] = $key;
                        $setStr['values'][] = '?';
                        break;
                    case 'update':
                        $setStr[] = "`$key` = ? ";
                        break;
                }

            }
        }

        if ($method === 'create') {
            $setStr = sprintf("(%s) VALUES (%s)", implode(',', $setStr['fields']), implode(',', $setStr['values']));
        }

        if ($method === 'update') {
            $setStr = implode(',', $setStr);
        }

        return $setStr;
    }

    private static function bindParams($fields)
    {
        $setParams = [];
        foreach (self::$allowed as $key) {
            if (!empty($fields[$key]) || $fields[$key] != "" || $fields[$key] != NULL) {
                $setParams[] = htmlspecialchars($fields[$key]);
            }

        }

        return $setParams;
    }

    public static function findBy($field, $value)
    {
        $connection = new Connection();
        $statement = $connection->dbh->prepare("SELECT id,name,email,username,created_at,password FROM users WHERE {$field} = ?");
        $statement->execute([$value]);

        return $statement->fetch(PDO::FETCH_OBJ);
    }

    public static function exists($field, $value, $id)
    {
        $connection = new Connection();
        $statement = $connection->dbh->prepare("SELECT id,name,email,username,created_at,password FROM users WHERE {$field} = ? AND id != ?");
        $statement->execute([$value, $id]);

        return $statement->fetch(PDO::FETCH_OBJ);
    }

    public static function all()
    {
        $connection = new Connection();
        $statement = $connection->dbh->prepare("SELECT * FROM users");
        $statement->execute();

        echo json_encode($statement->fetchAll(PDO::FETCH_OBJ));
    }

    public static function paginated($currentPage = 1, $pageSize = 10, $searchValue = '')
    {
        $offset = 0;

        if ($currentPage != 1) {
            $offset = ($currentPage * $pageSize) - $pageSize;
        }

        $connection = new Connection();
        $statement = $connection->dbh->prepare("SELECT id,name,email,username,created_at FROM users WHERE id != ? AND (name LIKE ? OR username LIKE ? OR email LIKE ?) ORDER BY created_at DESC LIMIT {$offset}, {$pageSize}");
        $statement->execute([session('authed_user')->id, "%{$searchValue}%", "%{$searchValue}%", "%{$searchValue}%"]);

        $users = $statement->fetchAll(PDO::FETCH_OBJ);

        $statement = $connection->dbh->prepare("SELECT COUNT(*) FROM users WHERE (name LIKE ? OR username LIKE ? OR email LIKE ?)");
        $statement->execute(["%{$searchValue}%", "%{$searchValue}%", "%{$searchValue}%"]);

        $totalRecords = $statement->fetch(PDO::FETCH_NUM)[0];

        $totalPages = ceil($totalRecords / $pageSize);

        return compact('users', 'totalRecords', 'totalPages');
    }

    public static function create($data)
    {
        $setStr = self::prepareFields($data);
        $bindParams = self::bindParams($data);

        try {
            $connection = new Connection();
            $statement = $connection->dbh->prepare("INSERT INTO users {$setStr}");
            $response = $statement->execute($bindParams);

            if ($response) {
                return $connection->dbh->lastInsertId();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function update($data, $id)
    {
        $setStr = self::prepareFields($data, 'update');
        $bindParams = self::bindParams($data);

        try {
            $connection = new Connection();
            $statement = $connection->dbh->prepare("UPDATE users SET {$setStr} WHERE id = {$id}");
            $response = $statement->execute($bindParams);

            return $response;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function delete($id)
    {
        try {
            $connection = new Connection();
            $statement = $connection->dbh->prepare("DELETE FROM users WHERE id = ?");

            return $statement->execute([$id]);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}