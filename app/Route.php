<?php


class Route
{
    public $name;
    public $pattern;
    public $class;
    public $method;
    public $params;

    /**
     * Route constructor.
     * @param $name
     * @param $pattern
     * @param $class
     * @param $method
     * @param $params
     */
    public function __construct($name, $pattern, $class, $method, $params = null)
    {
        $this->name = $name;
        $this->pattern = $pattern;
        $this->class = $class;
        $this->method = $method;
        $this->params = $params;
    }


}