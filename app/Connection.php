<?php


class Connection
{
    /**
     * @var PDO
     */
    public $dbh;


    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $dsn = 'mysql:dbname=' . $_ENV['DB_NAME'] . ';host=' . $_ENV['DB_HOST'];
        $username = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $this->dbh = new PDO($dsn, $username, $password);

        } catch (PDOException $e) {
            throw new Exception('Connection failure. Check host, database name, username or password.');
        }
    }
}