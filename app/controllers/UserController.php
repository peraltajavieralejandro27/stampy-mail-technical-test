<?php

require 'app/User.php';

class UserController
{


    /**
     * UserController constructor.
     */
    public function __construct()
    {
        isAuthed();
    }

    public function index()
    {
        require './resources/views/users/index.php';
    }

    public function show($id)
    {
        $user = User::findBy('id', $id);

        require './resources/views/users/show.php';
    }

    public function create()
    {
        require './resources/views/users/create.php';
    }

    public function store()
    {
        if (User::findBy('username', $_POST['username']) || User::findBy('email', $_POST['email'])) {
            setError('create_user_error', 'The username or email are already in use');

            redirect('user/create');

            return false;
        }

        if ($_POST['password'] !== $_POST['password_confirm']) {
            setError('create_user_error', "The passwords doesn't match");

            redirect('user/create');

            return false;
        }

        $_POST['password'] = encryptPassword($_POST['password']);

        if ($id = User::create($_POST)) {
            setAlert('user_created', 'User created successfully');

            redirect("user/find/{$id}");
        }
    }

    public function edit($id)
    {
        $user = User::findBy('id', $id);

        require './resources/views/users/edit.php';
    }

    public function update($id)
    {
        if (User::exists('username', $_POST['username'], $id) || User::exists('email', $_POST['email'], $id)) {
            setError('create_user_error', 'The username or email are already in use');

            redirect("user/edit/{$id}");

            return false;
        }

        if (isset($_POST['password']) && !empty($_POST['password'])) {
            if ($_POST['password'] !== $_POST['password_confirm']) {
                setError('create_user_error', "The passwords doesn't match");

                redirect("user/edit/{$id}");

                return false;
            }

            $_POST['password'] = encryptPassword($_POST['password']);
        }

        $_POST['updated_at'] = date('Y-m-d H:i:s');

        if (User::update($_POST, $id)) {

            setAlert('user_created', 'User updated successfully');

            redirect("user/find/{$id}");

            return true;
        }

        setError('create_user_error', "There was an error updating the user.");

        redirect('user/edit/' . $id);
    }

    public function delete($id)
    {
        $user = User::findBy('id', $id);

        if (User::delete($id)) {
            echo json_encode(['success' => true, 'message' => "User {$user->name} deleted successfully"]);
        }
    }

    public function fetch()
    {
        echo json_encode(User::paginated($_GET['current_page'], $_GET['page_size'], $_GET['search_value']));
    }

}