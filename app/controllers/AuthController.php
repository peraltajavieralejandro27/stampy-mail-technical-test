<?php

require 'app/User.php';

class AuthController
{


    public function index()
    {
        if (isset($_SESSION['authed_user']) && $_SESSION['authed_user']) {
            redirect('users');
        }

        require './resources/views/login/index.php';
    }

    public function store()
    {
        if (!$user = User::findBy('email', $_POST['email'])) {
            $user = User::findBy('username', $_POST['email']);
        }

        if ($user && checkPassword($_POST['password'], $user->password)) {
            $_SESSION['authed_user'] = $user;

            redirect('users');
        } else {
            setError('login_error', 'Wrong email or password.');

            redirect('login');
        }
    }

    public function delete()
    {
        $_SESSION['authed_user'] = null;

        session_destroy();

        redirect('login');
    }
}