<?php

require_once __DIR__ . '/app/Route.php';
require_once __DIR__ . '/app/Router.php';


$routes = [
//    new Route('', '/', 'AuthController', 'index'),
    new Route('login', '/login', 'AuthController', 'index'),
    new Route('auth', '/auth', 'AuthController', 'store'),
    new Route('logout', '/logout', 'AuthController', 'delete'),

    new Route('users', '/users', 'UserController', 'index'),
    new Route('user/create', '/user/create', 'UserController', 'create'),
    new Route('user/store', '/user/store', 'UserController', 'store'),
    new Route('user/find', '/user/find/', 'UserController', 'show'),
    new Route('user/edit', '/user/edit/', 'UserController', 'edit'),
    new Route('user/update', '/user/update/', 'UserController', 'update'),
    new Route('user/delete', '/user/delete/', 'UserController', 'delete'),
    new Route('fetch/users', '/fetch/users/', 'UserController', 'fetch'),
];

$router = new Router($routes);

$match = $router->resolve($_SERVER['REQUEST_URI']);

if ($match) {

    include __DIR__ . "/app/controllers/{$match->class}.php";

    call_user_func_array(
        [
            new $match->class, $match->method
        ],
        $match->params
    );
}
