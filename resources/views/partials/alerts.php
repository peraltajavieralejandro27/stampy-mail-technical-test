<?php if (alerts('all')): ?>

    <?php foreach (alerts('all') as $alert): ?>
        <div class="alert alert-success">
            <p><?= $alert ?></p>
        </div>
    <?php endforeach ?>
    <?php $_SESSION['alerts'] = null; ?>
<?php endif ?>

<?php if (errors('all')): ?>
    <?php foreach (errors('all') as $error): ?>
        <div class="alert alert-danger">
            <p><?= $error ?></p>
        </div>
    <?php endforeach ?>
    <?php $_SESSION['errors'] = null; ?>
<?php endif ?>
