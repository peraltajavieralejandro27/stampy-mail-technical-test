<?php require_once view('layouts/auth/header.php'); ?>

    <div class="login-container">

        <div class="login-box">
            <h1 class="login-title">Login</h1>

            <?php require view('partials/alerts.php') ?>

            <form action="<?= url('auth') ?>" method="post">
                <input
                        type="text"
                        id="email"
                        name="email"
                        placeholder="E-mail or username"
                        required
                        class="form-control mb-1"
                        autocomplete="off"
                        autofocus
                >

                <input
                        type="password"
                        id="password"
                        name="password"
                        placeholder="Password"
                        required
                        class="form-control mb-3"
                >

                <button
                        class="btn btn-primary btn-block mb-2"
                        type="submit"
                >
                    Login
                </button>

                <a href="#">Forgot my password</a>
            </form>
        </div>
    </div>

<?php require_once view('layouts/auth/footer.php'); ?>