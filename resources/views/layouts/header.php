<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#9898e2" />
    <title>Stampy Mail technical test</title>
    <link rel="stylesheet" href="/resources/assets/css/app.css?v=<?= $_ENV['CACHE_HASH'] ?>">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
</head>
<body>

<header class="nav-container">
    <nav>
        <ul>
            <li class="nav-brand" title="<?= session('authed_user')->email ?>">
                <a href="<?= url('user/edit/' . session('authed_user')->id) ?>">
                    <small>
                        <b><?= session('authed_user')->name ?></b>
                    </small>
                </a>
            </li>

            <li>
                <a href="<?= url('users') ?>" class="btn btn-white">Users</a>
            </li>
            <li class="pull-right">
                <a href="<?= url('logout') ?>" class="btn btn-white">Logout</a>
            </li>
        </ul>
    </nav>
</header>

<div class="container">
