<?php require view('partials/alerts.php') ?>

<div class="alert alert-info">
    <p><b>** All fields are required **</b></p>
</div>

<div class="mt-1">
    <label for="name">Name</label>
    <input type="text"
           name="name"
           id="name"
           placeholder="Name"
           value="<?= isset($user) ? $user->name : '' ?>"
           class="form-control mt-1"
           required>
</div>

<div class="mt-1">
    <label for="username">Username</label>
    <input type="text"
           name="username"
           id="username"
           placeholder="Username"
           value="<?= isset($user) ? $user->username : '' ?>"
           class="form-control mt-1"
           required
    >
</div>

<div class="mt-1">
    <label for="email">E-mail</label>
    <input type="email"
           name="email"
           id="email"
           placeholder="E-mail"
           value="<?= isset($user) ? $user->email : '' ?>"
           class="form-control mt-1"
           required
    >
</div>

<?php if (isset($user)): ?>
    <div class="alert alert-warning mt-2 mb-2">
        <p><b>If you don't want to update your password leave the next fields empty.</b></p>
    </div>
<?php endif ?>

<div class="mt-1">
    <label for="password">Password</label>
    <input type="password"
           name="password"
           id="password"
           placeholder="Password"
           class="form-control mt-1"
        <?= !isset($user) ? 'required' : '' ?>
    >
</div>

<div class="mt-1">
    <label for="password-confirm">Password confirmation</label>
    <input type="password"
           name="password_confirm"
           id="password-confirm"
           placeholder="Password confirmation"
           class="form-control mt-1"
        <?= !isset($user) ? 'required' : '' ?>
    >
</div>

<div class="row mt-5">
    <a href="<?= url('users') ?>" class="btn btn-secondary">
        Back
    </a>

    <button type="submit" class="btn btn-primary pull-right">
        <?= isset($user) ? 'Update' : 'Save' ?>
    </button>
</div>


<script>

</script>