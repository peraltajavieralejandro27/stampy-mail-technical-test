<?php require_once view('layouts/header.php'); ?>

    <div class="section-header">
        <ul>
            <li>
                <a href="<?= url('users') ?>">Users</a>&emsp;>&emsp;
            </li>
            <li>
                <b>Edit</b>&emsp;>&emsp;
            </li>
            <li>
                <b><?= $user->name ?></b>
            </li>
        </ul>
    </div>

    <div class="main-content mt-3">
        <form method="post" action="<?= url('user/update/'.$user->id) ?>">
            <?php require_once view('users/form.php'); ?>
        </form>
    </div>

<?php require_once view('layouts/footer.php'); ?>