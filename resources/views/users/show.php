<?php require_once view('layouts/header.php'); ?>


    <div class="section-header">
        <ul>
            <li>
                <a href="<?= url('users') ?>">Users</a>&emsp;>&emsp;
            </li>
            <li>
                <b>Show&emsp;>&emsp;</b>
            </li>
            <li>
                <b><?= $user->name ?></b>
            </li>
        </ul>
    </div>

    <div class="main-content mt-3">

        <?php require view('partials/alerts.php') ?>

        <div class="row">
            <h2>User information</h2>

            <a href="<?= url('user/edit/' . $user->id) ?>"
               class="btn btn-secondary pull-right btn-sm">
                Update information
            </a>
        </div>

        <div class="divider"></div>

        <div class="article mb-5">
            <p class="mb-1">Name: <strong><?= $user->name ?></strong></p>
            <p class="mb-1">Username: <strong><?= $user->username ?></strong></p>
            <p class="mb-1">E-mail: <strong><?= $user->email ?></strong></p>
            <p class="mb-1">Created at: <strong><?= $user->created_at ?></strong></p>
        </div>

        <a href="<?= url('users') ?>" class="btn btn-secondary">
            Back
        </a>

    </div>

<?php require_once view('layouts/footer.php'); ?>