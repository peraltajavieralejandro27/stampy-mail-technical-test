<?php require_once view('layouts/header.php'); ?>

    <div class="section-header">
        <ul>
            <li>
                <b>Users</b>
            </li>
        </ul>
    </div>

    <div class="main-content mt-3">

        <form id="users-search-form" class="mb-3">
            <div class="row row-xs-column">
                <div class="col-6 col-xs-12">
                    <input type="text" name="search_value" placeholder="User search" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary btn-xs-block ml-2 ml-xs-0 mt-xs-1">Search</button>

                <a href="<?= url('user/create') ?>" class="btn btn-secondary btn-xs-block mt-xs-1 pull-right">Create user</a>
            </div>
        </form>

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="users-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>User name</th>
                    <th>E-mail</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody></tbody>
            </table>
        </div>

        <div class="row mt-2">
            <ul id="table-pagination" class="table-pagination pagination-container row justify-content-center flex-flow-wrap"></ul>
        </div>

    </div>

    <script src="/resources/assets/js/users.js?v=<?= $_ENV['CACHE_HASH'] ?>"></script>

<?php require_once view('layouts/footer.php'); ?>