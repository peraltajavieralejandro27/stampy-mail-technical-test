let currentPage = 1;
let pageSize = 10;
let searchValue = document.querySelector('[name=search_value]');
const usersTableTBody = document.getElementById('users-table').getElementsByTagName('tbody')[0];
const usersTablePagination = document.getElementById('table-pagination');
const form = document.getElementById('users-search-form');

const fetchUsers = async (selectedPage = 1, searchValue = null) => {
    const response = await fetch(`/fetch/users/?current_page=${selectedPage}&page_size=${pageSize}&search_value=${searchValue}`);

    return response.json();
}

const tableRender = (selectedPage = 1, searchValue = '') => {

    currentPage = selectedPage;

    fetchUsers(selectedPage, searchValue).then(({users, totalPages, totalRecords}) => {

        usersTableTBody.innerHTML = '';

        users.forEach(user => {
            usersTableTBody.insertAdjacentHTML('beforeend', `
                        <tr>
                            <td>${user.name}</td>
                            <td>${user.username}</td>
                            <td>${user.email}</td>
                            <td>${user.created_at}</td>
                            <td class="text-center">
                                <button onclick="window.location.href = '/user/find/${user.id}'" class="btn btn-default btn-sm">Show</button>
                                <button  onclick="window.location.href = '/user/edit/${user.id}'" class="btn btn-info btn-sm">Edit</button>
                                <button class="btn btn-danger btn-sm" onclick="deleteUser(${user.id})">Delete</button>
                            </td>
                        </tr>
                    `)

        });

        usersTablePagination.innerHTML = '';

        if(totalPages > 1){
            usersTablePagination.insertAdjacentHTML('beforeend', `
                        <li class="btn btn-secondary btn-sm mr-05 mt-xs-1 ${currentPage === 1 ? 'disabled-link' : ''}" onclick="tableRender(${currentPage - 1},searchValue.value)">
                            <a href="javascript:void(0);"> < </a>
                        </li>
                `);
        }

        for (let i = 1; i <= totalPages; i++) {
            usersTablePagination.insertAdjacentHTML('beforeend', `
                        <li class="btn btn-secondary btn-sm mr-05 mt-xs-1 ${i === currentPage ? 'disabled-link' : ''}" onclick="tableRender(${i},searchValue.value)">
                            <a href="javascript:void(0);"> ${i}</a>
                        </li>
                `);
        }

        if(totalPages > 1){
            usersTablePagination.insertAdjacentHTML('beforeend', `
                        <li class="btn btn-secondary btn-sm mr-05 mt-xs-1 ${currentPage === totalPages ? 'disabled-link' : ''}" onclick="tableRender(${currentPage + 1},searchValue.value)">
                            <a href="javascript:void(0);"> > </a>
                        </li>
                `);
        }
    });
}

const searchUser = (event) => {
    event.preventDefault();

    tableRender(1, searchValue.value);

}

const deleteUser = async (id) => {
    const response = await fetch(`user/delete/${id}`);

    response.json().then(({success, message}) => {
        if (success) {
            tableRender();
        }
    });
}

tableRender();


form.addEventListener('submit', (searchUser));
