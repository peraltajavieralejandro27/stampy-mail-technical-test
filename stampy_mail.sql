-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for stampy_mail
CREATE DATABASE IF NOT EXISTS `stampy_mail` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `stampy_mail`;

-- Dumping structure for table stampy_mail.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  UNIQUE KEY `users_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=359 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stampy_mail.users: ~64 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `username`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'Javier Alejandro Peralta', 'peraltajavieralejandro@gmail.com', 'Javier Alejandro Peralta3', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-03 22:34:27', '2021-02-03 22:34:27'),
	(247, 'Marques', 'kertzmann.norwood@example.com', 'Cole', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:01', '2021-02-04 21:25:01'),
	(252, 'Gladyce', 'candelario.kris@example.net', 'Kuhlman', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:01', '2021-02-04 21:25:01'),
	(253, 'Gina', 'lilyan26@example.com', 'Roob', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:01', '2021-02-04 21:25:01'),
	(254, 'Lessie', 'imetz@example.com', 'Corwin', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:01', '2021-02-04 21:25:01'),
	(255, 'Wilhelmine', 'marjory.rolfson@example.com', 'Harbers', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:01', '2021-02-04 21:25:01'),
	(256, 'Antwan', 'lia.lindgren@example.com', 'Becker', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(265, 'Nolan', 'ofay@example.net', 'Harber', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(266, 'Ford', 'yesenia.thompson@example.org', 'Tremblay', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(267, 'Ryann', 'yazmin.stiedemann@example.org', 'Wyman', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(268, 'Daphnee', 'little.janae@example.org', 'Willms', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(269, 'Connie', 'will.shayne@example.net', 'O\'Conner', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(270, 'Marion', 'raynor.unique@example.com', 'Ernser', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(271, 'Tia', 'silas04@example.org', 'Effertz', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(272, 'Vickie', 'everett.sanford@example.org', 'Pfeffer', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(273, 'Nathanial', 'kamren.quitzon@example.org', 'Weber', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(274, 'Deangelo', 'wilton.doyle@example.org', 'Sipes', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(275, 'Don', 'brendan.mckenzie@example.net', 'Heller', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(276, 'Kariane', 'emiliano.franecki@example.org', 'Treutel', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(277, 'Angelo', 'roxane.kessler@example.org', 'Parker', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(278, 'Kayley', 'beier.desmond@example.net', 'Schaefer', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(279, 'Keith', 'marcellus.prohaska@example.net', 'Herman', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(280, 'Emory', 'weimann.kraig@example.org', 'Abernathy', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(281, 'Ebba', 'brown.haag@example.net', 'Welch', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(282, 'Hoyt', 'richard.champlin@example.net', 'Larkin', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(283, 'Doug', 'lreichel@example.org', 'Beatty', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(284, 'Hulda', 'danyka95@example.net', 'Rowe', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(285, 'Juliet', 'anika.hoeger@example.com', 'Daugherty', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(286, 'Randy', 'legros.polly@example.net', 'Pacocha', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(287, 'Breana', 'freida.gutkowski@example.com', 'Welch2', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(288, 'Keyshawn', 'west.moshe@example.com', 'Miller', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(289, 'Linnea', 'mreinger@example.org', 'Harvey', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(290, 'Deborah', 'elyssa.stanton@example.net', 'Jaskolski', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(291, 'Cathryn', 'karley46@example.org', 'Effertz2', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(292, 'Jessy', 'gaylord.orpha@example.org', 'Doyle', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(293, 'Carmelo', 'mack.lakin@example.com', 'Huels', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(294, 'Vella', 'uriel04@example.net', 'Kohler', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(295, 'Otis', 'schimmel.brandy@example.org', 'Mitchell', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(296, 'Margarita', 'santos63@example.net', 'Will22', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(297, 'Samir', 'noah.crona@example.com', 'Welch4', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(298, 'Milan', 'pstracke@example.net', 'Mueller', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(299, 'Lamont', 'magnolia03@example.net', 'Prohaska', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(300, 'Mozelle', 'stan.hammes@example.org', 'Ondricka', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(301, 'Joe', 'ftorp@example.com', 'Kerluke', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(302, 'Mike', 'weissnat.nettie@example.com', 'Frami', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(303, 'Talon', 'kirlin.kolby@example.org', 'Corkery', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(304, 'Ava', 'marcelina22@example.org', 'Casper', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(305, 'Edna', 'kertzmann.brianne@example.com', 'Kozey', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(306, 'Lilla', 'boehm.jadon@example.org', 'Jerde', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(307, 'Tyreek', 'kemmer.bridgette@example.org', 'McClure', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(308, 'Millie', 'electa75@example.org', 'Schuster', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(309, 'Leda', 'shamill@example.com', 'Bernhard', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(310, 'Saul', 'koelpin.hoyt@example.org', 'Lemke', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(311, 'Daisha', 'ydooley@example.org', 'Kuphal', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(312, 'Forest', 'libby.hoppe@example.org', 'Langosh', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(313, 'Immanuel', 'mandy32@example.net', 'Nader', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(314, 'Astrid', 'krista17@example.com', 'Steuber', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(315, 'Demarcus', 'gleason.gregoria@example.com', 'Rohan', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(316, 'Elza', 'lhalvorson@example.org', 'Breitenberg', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(317, 'Owen', 'curt94@example.org', 'Wiza', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(318, 'Leonard', 'elton72@example.com', 'Kilback', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(319, 'Pablo', 'ines04@example.org', 'Kassulke', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(320, 'Carmine', 'krajcik.mollie@example.net', 'Ward', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02'),
	(321, 'Kasey', 'muller.deonte@example.net', 'Homenick', '$2y$10$kF3OPcEfF05TZehJb6Pcsu7C404VknRZKtoge8hM1/ePjQ7A8j0h.', '2021-02-04 21:25:02', '2021-02-04 21:25:02');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
