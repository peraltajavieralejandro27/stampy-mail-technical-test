<?php

session_start();

require_once './config/cache.php';

require_once './config/database.php';

require_once './app/Connection.php';

require_once './app/helpers.php';

require_once './routes.php';